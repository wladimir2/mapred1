import java.io.IOException;
import java.lang.Math;
import java.util.StringTokenizer;
import java.util.Scanner;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
import java.util.LinkedList;
import java.util.TreeSet;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.Collections;
import java.util.Comparator;
import java.net.URL;
import java.net.MalformedURLException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.KeyValueTextInputFormat;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class WordCount {

  public static <K, V> String printMap(Map<K, V> map) {
    String s = new String();
    for (Map.Entry<K, V> entry : map.entrySet()) {
      s=s + entry.getKey() + "" + entry.getValue();
    }
    return s;
  }

  public static class TokenizerMapper
       extends Mapper<Object, Text, Text, Text>{
    // Na vyhode Key=parametr, Value=preobrazovannyi_URL
    // Preobrazovannyi_URL - eto URL, soderjashii etot parametr, v kotorom
    // dobavlen port, otsortirovany parametry, udaleny znaki & i =
    private Text paramText = new Text();
    private Text uniqueURL = new Text();

    public void map(Object key, Text value, Context context
                    ) throws IOException, InterruptedException {
      URL u;
      String s = new String(value.toString());
      String[] m = s.split(" ");
      try {
        //if(m.length == 7)
        u = new URL(m[m.length-1]);
        //else throw new MalformedURLException(Integer.toString(m.length));
      }
      catch(MalformedURLException ex){
        //System.out.println(ex.toString());
        return;
      }

      if(u.getQuery()!=null){
        String[] mass;
        String[] subMass;
        Map<String, String> tm = new TreeMap<String, String>();
        ArrayList<String> arr = new ArrayList<String>();
        String sss = u.getQuery();
        mass = sss.split("&");
        for (String t : mass) {
            subMass = t.split("=");
            if(subMass.length == 2) {
              tm.put(subMass[0], subMass[1]); //Integer.parseInt(s);
              arr.add(subMass[0]);
            }
        }
        s=u.getProtocol()+"://"+
              (u.getUserInfo()!=null?u.getUserInfo()+"@":"")+
              (u.getHost()!=null?u.getHost():"")+":"+
              (u.getPort()==-1?u.getDefaultPort():u.getPort())+
               u.getPath()+
              "?"+printMap(tm)+
              (u.getRef()!=null?"#"+u.getRef():"");

        for (String param : arr) {
          paramText.set(param);
          uniqueURL.set(s); //Integer.toString(m.length)
          context.write(paramText,uniqueURL);
        }

      }
    }
  }

  public static class IntSumReducer
       extends Reducer<Text,Text,Text,Text> {
    private Text dummy = new Text();
    private Text result = new Text();
    // Na vyhode Key="1", Value="Parametr Koli4esto_unikal'nyh_URL,
    // Koli4esto_unikal'nyh_URL - koli4esto URL, v kotoryh prisutstvuet etot parametr
    // Eto sdelano, 4toby pary (Key,Value) popali v vse vmeste v odin i tot je instance
    // Reducer2.class i byli otsortirovany
    public void reduce(Text key, Iterable<Text> values,
                       Context context
                       ) throws IOException, InterruptedException {
      HashSet<Text> hs = new HashSet<>();
      for (Text val : values) hs.add(val);
      result.set(key.toString()+" "+Integer.toString(hs.size()));
      dummy.set("1");
      context.write(dummy, result);
    }
  }

  public static class Mapper2
       extends Mapper<Text, Text, Text, Text>{

    public void map(Text key, Text value, Context context
                    ) throws IOException, InterruptedException {
      context.write(key, value);
    }
  }

  public static class KVRecord {
       String str;
       Integer number;

       KVRecord(String str, Integer number) {
             this.str = str;
             this.number = number;
       }
       public String getStr(){return str;}
       public Integer getNumber(){return number;}
  }

  public static class Comp implements Comparator<KVRecord> {
       public int compare(KVRecord kvr1, KVRecord kvr2) {
         Integer diff = kvr2.getNumber() - kvr1.getNumber();
         int result = diff.intValue();
         if(result != 0) return (int) result / Math.abs( result );
         return 0;
       }
  }

  public static class Reducer2
        extends Reducer<Text,Text,Text,IntWritable> {
    private IntWritable result = new IntWritable();
    private Text param = new Text();

    public void reduce(Text key, Iterable<Text> values,
                       Context context
                       ) throws IOException, InterruptedException {

      ArrayList<KVRecord> al = new ArrayList<KVRecord>();

      for (Text val : values) {
        String[] mass;
        String sss = val.toString();
        mass = sss.split(" ");
        if(mass.length == 2) {
          al.add(new KVRecord(mass[0], Integer.parseInt(mass[1])));
          param.set(mass[0]);
        }
      }
      //result.set(al.size());
      //context.write(param, result);

      Collections.sort(al,new Comp());
      for (int i=0; i < 10; i++) {
        param.set(al.get(i).getStr());
        result.set(al.get(i).getNumber().intValue());
        context.write(param, result);
      }
    }
  }

  public static void main(String[] args) throws Exception {
    Configuration conf = new Configuration();
    Job job1 = Job.getInstance(conf);
    job1.setJarByClass(WordCount.class);
    job1.setJobName("WordCount");

    FileInputFormat.addInputPath(job1, new Path(args[0]));
    FileOutputFormat.setOutputPath(job1, new Path("/tmp/hw2/ou"));

    job1.setMapperClass(TokenizerMapper.class);
    //job1.setCombinerClass(IntSumReducer.class);
    job1.setReducerClass(IntSumReducer.class);

    job1.setMapOutputKeyClass(Text.class);
    job1.setMapOutputValueClass(Text.class);
    job1.setOutputKeyClass(Text.class);
    job1.setOutputValueClass(Text.class);
    //job1.setInputFormat(TextInputFormat.class);
    //job1.setOutputFormat(TextOutputFormat.class);
    job1.waitForCompletion(true);

    conf.set("mapreduce.output.textoutputformat.separator",":");
    Job job2 = Job.getInstance(conf);
    job2.setJarByClass(WordCount.class);
    job2.setJobName("TotalWords");

    job2.setInputFormatClass(KeyValueTextInputFormat.class);
    KeyValueTextInputFormat.addInputPath(job2, new Path("/tmp/hw2/ou/part-r-00000"));
    TextOutputFormat.setOutputPath(job2, new Path(args[1]));

    job2.setMapperClass(Mapper2.class);
    job2.setReducerClass(Reducer2.class);

    job2.setMapOutputKeyClass(Text.class);
    job2.setMapOutputValueClass(Text.class);
    job2.setOutputKeyClass(Text.class);
    job2.setOutputValueClass(IntWritable.class);
    job2.waitForCompletion(true);
  }
}
